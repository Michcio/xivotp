all: build dist

build:
    npm run build

dist:
    git -C dist add .
    git -C dist commit
    git push origin pages